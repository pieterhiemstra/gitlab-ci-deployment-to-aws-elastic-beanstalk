<?php

namespace SamplePhpApp\Controllers;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class UserController
{
    private $users = [
        1 => [
            'first_name' => 'Mike',
            'last_name' => 'Litoris'
        ],
        2 => [
            'first_name' => 'Peter',
            'last_name' => 'Ennis'
        ],
        3 => [
            'first_name' => 'Thomas',
            'last_name' => 'Fister'
        ],
    ];

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function index(Request $request, Response $response, array $args)
    {
        $response->getBody()->write(json_encode($this->users));
        return $response;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function show(Request $request, Response $response, array $args)
    {
        $response->getBody()->write(json_encode($this->users[$args['id']]));
        return $response;
    }
}
