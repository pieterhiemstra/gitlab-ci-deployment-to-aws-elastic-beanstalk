<?php
die('staging');
require __DIR__ . '/../vendor/autoload.php';

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;

$app = AppFactory::create();

$app->get('/', function (Request $request, Response $response, array $args) {
    $homeController = new \SamplePhpApp\Controllers\HomeController();

    return $homeController->index($request, $response, $args);
});

$app->get('/users', function (Request $request, Response $response, array $args) {
    $userController = new \SamplePhpApp\Controllers\UserController();

    return $userController->index($request, $response, $args);
});

$app->get('/users/{id}', function (Request $request, Response $response, array $args) {
    $userController = new \SamplePhpApp\Controllers\UserController();

    return $userController->show($request, $response, $args);
});

$app->run();
